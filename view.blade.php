<!-- Start Features 75 -->
@if(!Fir\Utils\Helpers::isProduction())
<!-- Description : Even width blocks that scroll horizontally as needed. -->
@endif
<div class="features-75"  is="fir-features-75">
  <script type="application/json">
      {
          "options": @json($options)
      }
  </script>
  <div class="features-75__wrap">
    @foreach($blocks as $block)
    <div class="features-75__block">
      @if($block['icon'])
      <img src="{{ $block['icon']['url'] }}" alt="{{ $block['title']}}" class="features-75__block-icon">
      @endif
      @if($block['title'])
      <h3 class="features-75__block-title">{{ $block['title'] }}</h3>
      @endif
      @if($block['text'])
      <p class="features-75__block-text">{{ $block['text'] }}</p>
      @endif
    </div>
    @endforeach
  </div>
</div>
<!-- End Features 75 -->

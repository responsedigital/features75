<?php

namespace Fir\Pinecones\Features75;

use Fir\Utils\GlobalFields;

class Schema
{
    public static function getACFLayout()
    {
        return [
            'name' => 'Features75',
            'label' => 'Pinecone: Features 75',
            'sub_fields' => [
                [
                    'label' => 'General',
                    'name' => 'generalTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Overview',
                    'name' => 'overview',
                    'type' => 'message',
                    'message' => "Even width blocks that scroll horizontally as needed."
                ],
                [
                    'label' => 'Blocks',
                    'name' => 'blocksTab',
                    'type' => 'tab',
                    'placement' => 'left',
                    'endpoint' => 0,
                ],
                [
                    'label' => 'Blocks',
                    'name' => 'blocks',
                    'type' => 'repeater',
                    'layout' => 'row',
                    'min' => 4,
                    'sub_fields' => [
                        [
                            'label' => 'Icon',
                            'name' => 'icon',
                            'type' => 'image'
                        ],
                        [
                            'label' => 'Title',
                            'name' => 'title',
                            'type' => 'text'
                        ],
                        [
                            'label' => 'Text',
                            'name' => 'text',
                            'type' => 'textarea'
                        ]
                    ]
                ],
                GlobalFields::getGroups(array(
                    'priorityGuides',
                    'ID'
                )),
                [
                    'label' => 'Options',
                    'name' => 'optionsTab',
                    'type' => 'tab',
                    'placement' => 'top',
                    'endpoint' => 0
                ],
                [
                    'label' => '',
                    'name' => 'options',
                    'type' => 'group',
                    'layout' => 'row',
                    'sub_fields' => [
                        GlobalFields::getOptions(array(
                            'hideComponent'
                        ))
                    ]
                ]
            ]
        ];
    }
}

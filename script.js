class Features75 extends window.HTMLDivElement {

    constructor (...args) {
        const self = super(...args)
        self.init()
        return self
    }

    init () {
        this.props = this.getInitialProps()
        this.resolveElements()
      }
    
    getInitialProps () {
        let data = {}
        try {
            data = JSON.parse($('script[type="application/json"]', this).text())
        } catch (e) {}
        return data
    }

    resolveElements () {

    }

    connectedCallback () {
        this.initFeatures75()
    }

    initFeatures75 () {
        const { options } = this.props
        const config = {

        }

        // console.log("Init: Features 75")
    }

}

window.customElements.define('fir-features-75', Features75, { extends: 'div' })

module.exports = {
    'name'  : 'Features 75',
    'camel' : 'Features75',
    'slug'  : 'features-75',
    'dob'   : 'Features_75_375',
    'desc'  : 'Even width blocks that scroll horizontally as needed.',
}